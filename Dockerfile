FROM ubuntu:latest as user
RUN useradd developer

FROM alpine:latest as builder
RUN apk --no-cache add tzdata

FROM scratch
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=user /etc/passwd /etc/passwd
COPY ./configs ./configs
COPY netology-test-task .
USER developer
CMD ["./netology-test-task"]