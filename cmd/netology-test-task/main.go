package main

import (
	"flag"

	app "gitlab/netology-test-task/internal/application"
)

var opt = new(app.Options)

func init() {
	flag.IntVar(&opt.Port, "port", 8080, "app port")
	flag.StringVar(&opt.Env, "env", "prod", "app environment")
	flag.BoolVar(&opt.IsPprof, "pprof", false, "the state of the profiler, on - true/off - false")
	flag.StringVar(&opt.LogOutputFilePath, "out", "", "log output file path")
	flag.Parse()
}

func main() {
	app.NewApplication(opt).Start()
}
