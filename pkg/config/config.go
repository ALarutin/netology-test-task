package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type (
	Config struct {
		Application Application `json:"application"`
	}
)

func NewConfig(configFilePath string) *Config {
	bytes, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		log.Panicf("can't read file by path: %s with error: %s", configFilePath, err.Error())
	}

	var c = new(Config)
	if err := json.Unmarshal(bytes, c); err != nil {
		log.Panicf("can't unmarshal files bytes by path: %s with error: %s", configFilePath, err.Error())
	}

	return c
}
