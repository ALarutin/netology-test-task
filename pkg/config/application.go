package config

type (
	Application struct {
		Name            string   `json:"name"`
		Version         string   `json:"version"`
		Timeout         Duration `json:"timeout"`
		ShutdownTimeout Duration `json:"shutdown_timeout"`
	}
)
