package config

import (
	"fmt"
	"time"
)

type (
	Duration time.Duration
)

func (d *Duration) UnmarshalJSON(durationBytes []byte) error {
	if string(durationBytes) == "null" {
		return nil
	}

	durationStr, err := unquoteIfQuoted(durationBytes)
	if err != nil {
		return err
	}

	duration, err := time.ParseDuration(durationStr)
	if err != nil {
		return fmt.Errorf("can't parse json: %s; with error: %s", durationStr, err.Error())
	}

	*d = Duration(duration)

	return nil
}

func (d *Duration) Cast() time.Duration {
	return time.Duration(*d)
}

func unquoteIfQuoted(bytes []byte) (string, error) {
	if len(bytes) <= 2 {
		return "", fmt.Errorf("invalidated bytes to unquoted")
	}

	if bytes[0] == '"' && bytes[len(bytes)-1] == '"' {
		bytes = bytes[1 : len(bytes)-1]
	}

	return string(bytes), nil
}
