package logger

import (
	"fmt"
	"io"
	"log"
	"os"

	"github.com/xlab/closer"
)

func NewLogger(logFilePath string) *log.Logger {
	return log.New(getWriter(logFilePath), "", log.Llongfile)
}

func getWriter(logFilePath string) io.Writer {
	if logFilePath == "" {
		return os.Stdout
	}

	w, err := os.OpenFile(logFilePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return os.Stdout
	}

	closer.Bind(func() {
		fmt.Println(w.Close())
	})

	return w
}
