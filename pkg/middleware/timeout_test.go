package middleware

import (
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/gorilla/mux"
)

const sleepDuration = time.Millisecond * 10

func timeoutHandler(w http.ResponseWriter, r *http.Request) { time.Sleep(sleepDuration) }

func getTimeoutHandler() http.Handler {
	router := mux.NewRouter()
	router.HandleFunc("/", timeoutHandler).Methods("GET")
	return router
}

func Test_middleware_Timeout(t *testing.T) {
	type (
		fields struct {
			logger  *log.Logger
			timeout time.Duration
		}

		args struct {
			next http.Handler
			resp *httptest.ResponseRecorder
			r    *http.Request
		}

		expected struct {
			body           string
			status         int
			isUnsuccessful bool
		}
	)
	tests := []struct {
		name     string
		fields   fields
		args     args
		expected expected
	}{
		{
			name: "Successful request",
			fields: fields{
				logger:  log.New(os.Stderr, "", log.Llongfile),
				timeout: sleepDuration * 2,
			},
			args: args{
				next: getTimeoutHandler(),
				resp: httptest.NewRecorder(),
				r:    httptest.NewRequest(http.MethodGet, "/", nil),
			},
			expected: expected{
				status: http.StatusOK,
			},
		},
		{
			name: "Unsuccessful too long",
			fields: fields{
				logger:  log.New(os.Stderr, "", log.Llongfile),
				timeout: sleepDuration / 2,
			},
			args: args{
				next: getTimeoutHandler(),
				resp: httptest.NewRecorder(),
				r:    httptest.NewRequest(http.MethodGet, "/", nil),
			},
			expected: expected{
				body:           `{"error":"timeout too long"}`,
				status:         http.StatusBadRequest,
				isUnsuccessful: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &middleware{
				logger:  tt.fields.logger,
				timeout: tt.fields.timeout,
			}

			timeoutMiddleware := m.Timeout(tt.args.next)
			timeoutMiddleware.ServeHTTP(tt.args.resp, tt.args.r)

			if tt.args.resp.Code != tt.expected.status {
				t.Errorf("got status: %d;expected: %d", tt.args.resp.Code, tt.expected.status)
			}

			if tt.expected.isUnsuccessful {
				if gotHeader := tt.args.resp.Header().Get("Content-Type"); !strings.Contains(gotHeader, "application/json") {
					t.Errorf("got needed Content-Type header: %s;expected: %s", gotHeader, "application/json")
				}
			}

			if gotBody := tt.args.resp.Body.String(); !strings.EqualFold(gotBody, tt.expected.body) {
				t.Errorf("got body: %s;expected: %s", gotBody, tt.expected.body)
			}
		})
	}
}
