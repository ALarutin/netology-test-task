package middleware

import (
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gorilla/mux"
)

func panicHandler(w http.ResponseWriter, r *http.Request) { panic("some panic") }

func getPanicHandler() http.Handler {
	router := mux.NewRouter()
	router.HandleFunc("/", panicHandler).Methods("GET")
	return router
}

func Test_middleware_Recover(t *testing.T) {
	type (
		fields struct {
			logger  *log.Logger
		}

		args struct {
			next http.Handler
			resp *httptest.ResponseRecorder
			r    *http.Request
		}

		expected struct {
			status int
		}
	)
	tests := []struct {
		name     string
		fields   fields
		args     args
		expected expected
	}{
		{
			name: "Successful catch panic",
			fields: fields{
				logger:  log.New(os.Stderr, "", log.Llongfile),
			},
			args: args{
				next: getPanicHandler(),
				resp: httptest.NewRecorder(),
				r:    httptest.NewRequest(http.MethodGet, "/", nil),
			},
			expected: expected{
				status: http.StatusInternalServerError,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &middleware{
				logger:  tt.fields.logger,
			}

			recoverMiddleware := m.Recover(tt.args.next)
			recoverMiddleware.ServeHTTP(tt.args.resp, tt.args.r)

			if tt.args.resp.Code != tt.expected.status {
				t.Errorf("got status: %d;expected: %d", tt.args.resp.Code, tt.expected.status)
			}
		})
	}
}
