package middleware

import (
	"bytes"
	"net/http"
	"sync"
)

type (
	timeoutWriter struct {
		mu  sync.Mutex
		buf bytes.Buffer

		header http.Header
		status int

		wrStatus bool
	}
)

func (tw *timeoutWriter) Write(p []byte) (int, error) {
	tw.mu.Lock()
	defer tw.mu.Unlock()
	return tw.buf.Write(p)
}

func (tw *timeoutWriter) Header() http.Header {
	return tw.header
}

func (tw *timeoutWriter) WriteHeader(code int) {
	tw.mu.Lock()
	defer tw.mu.Unlock()
	tw.wrStatus = true
	tw.status = code
}
