package middleware

import (
	"net/http"
	"runtime/debug"
)

// Данный middleware отлавливает panic
func (m *middleware) Recover(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				m.logger.Printf("caught panic: %v\n%s", r, debug.Stack())
				w.WriteHeader(http.StatusInternalServerError)
			}
		}()

		next.ServeHTTP(w, r)
	})
}
