package middleware

import (
	"log"
	"net/http"
	"time"
)

type (
	Middleware interface {
		recoverMiddlewareGetter
		timeoutMiddlewareGetter
	}
	recoverMiddlewareGetter interface {
		Recover(next http.Handler) http.Handler
	}
	timeoutMiddlewareGetter interface {
		Timeout(next http.Handler) http.Handler
	}

	middleware struct {
		logger  *log.Logger
		timeout time.Duration
	}
)

func NewMiddleware(logger *log.Logger, timeout time.Duration) Middleware {
	return &middleware{
		logger:  logger,
		timeout: timeout,
	}
}
