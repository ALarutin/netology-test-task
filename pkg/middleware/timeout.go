package middleware

import (
	"context"
	"io"
	"net/http"
)

// Данный middleware это упрощенный вариант функции http.TimeoutHandler
// Из-за ТЗ о http.StatusBadRequest, пришлось почти полностью повторить функционал стандартной библиотеки
// Для данного кейса, на prod-е я бы использовал http.TimeoutHandler
func (m *middleware) Timeout(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx, cancelCtx := context.WithTimeout(r.Context(), m.timeout)
		defer cancelCtx()

		r = r.WithContext(ctx)

		var (
			tw = &timeoutWriter{
				header: make(http.Header),
			}
			done = make(chan struct{})
		)

		go func() {
			defer close(done)
			next.ServeHTTP(tw, r)
		}()

		select {
		case <-ctx.Done():
			tw.mu.Lock()
			defer tw.mu.Unlock()

			w.Header().Set("Content-Type", "application/json")

			// Если ссылаться на rfc2616, здесь нужно отдать http.StatusRequestTimeout (408)
			// https://tools.ietf.org/html/rfc2616#section-10.4.9
			w.WriteHeader(http.StatusBadRequest)

			if _, err := io.WriteString(w, `{"error":"timeout too long"}`); err != nil {
				m.logger.Printf("can't write into response with error: %s\n", err.Error())
			}
		case <-done:
			tw.mu.Lock()
			defer tw.mu.Unlock()

			for k, vv := range tw.header {
				w.Header()[k] = append(vv[:0:0], vv...)
			}

			if !tw.wrStatus {
				tw.status = http.StatusOK
			}

			w.WriteHeader(tw.status)
			if _, err := w.Write(tw.buf.Bytes()); err != nil {
				m.logger.Printf("can't write into response with error: %s\n", err.Error())
			}
		}
	})
}
