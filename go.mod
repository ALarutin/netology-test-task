module gitlab/netology-test-task

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/prometheus/client_golang v1.8.0
	github.com/xlab/closer v0.0.0-20190328110542-03326addb7c2
)
