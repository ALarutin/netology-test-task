package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab/netology-test-task/internal/controller"
	"gitlab/netology-test-task/pkg/middleware"
)

func NewRouter(c controller.Controller, m middleware.Middleware) http.Handler {
	router := mux.NewRouter()

	router.
		Name("Sleep").
		Methods(http.MethodPost).
		Path("/api/slow").
		HandlerFunc(c.Sleep)

	router.Use(m.Timeout, m.Recover)

	router.
		Name("prometheus").
		Methods(http.MethodGet).
		Path("/metrics").
		Handler(promhttp.Handler())

	return router
}
