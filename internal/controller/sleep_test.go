package controller

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

type (
	mockIoReader struct{}
)

func (mockIoReader) Read([]byte) (n int, err error) {
	return 0, fmt.Errorf("mock error")
}

func Test_controller_Sleep(t *testing.T) {
	type (
		fields struct {
			logger *log.Logger
		}

		args struct {
			resp *httptest.ResponseRecorder
			r    *http.Request
		}

		expected struct {
			body   string
			status int
		}
	)

	var (
		tests = []struct {
			name     string
			fields   fields
			args     args
			expected expected
		}{
			{
				name:   "Successful request",
				fields: fields{logger: log.New(os.Stderr, "", log.Llongfile)},
				args: args{
					resp: httptest.NewRecorder(),
					r:    httptest.NewRequest(http.MethodPost, "/api/slow", bytes.NewBuffer([]byte(`{"timeout":1}`))),
				},
				expected: expected{
					body:   `{"status":"ok"}`,
					status: http.StatusOK,
				},
			},
			{
				name:   "Unsuccessful readall error",
				fields: fields{logger: log.New(os.Stderr, "", log.Llongfile)},
				args: args{
					resp: httptest.NewRecorder(),
					r:    httptest.NewRequest(http.MethodPost, "/api/slow", new(mockIoReader)),
				},
				expected: expected{
					body:   `{"error":"invalidate request body"}`,
					status: http.StatusBadRequest,
				},
			},
			{
				name:   "Unsuccessful empty bytes",
				fields: fields{logger: log.New(os.Stderr, "", log.Llongfile)},
				args: args{
					resp: httptest.NewRecorder(),
					r:    httptest.NewRequest(http.MethodPost, "/api/slow", bytes.NewBuffer(make([]byte, 0))),
				},
				expected: expected{
					body:   `{"error":"empty requests body"}`,
					status: http.StatusBadRequest,
				},
			},
			{
				name:   "Unsuccessful invalidate json",
				fields: fields{logger: log.New(os.Stderr, "", log.Llongfile)},
				args: args{
					resp: httptest.NewRecorder(),
					r:    httptest.NewRequest(http.MethodPost, "/api/slow", bytes.NewBuffer([]byte(`///////`))),
				},
				expected: expected{
					body:   `{"error":"invalidate json"}`,
					status: http.StatusBadRequest,
				},
			},
		}
	)

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var (
				c = &controller{
					logger: tt.fields.logger,
				}
			)

			c.Sleep(tt.args.resp, tt.args.r)

			if tt.args.resp.Code != tt.expected.status {
				t.Errorf("got status: %d;expected: %d", tt.args.resp.Code, tt.expected.status)
			}

			if gotHeader := tt.args.resp.Header().Get("Content-Type"); !strings.Contains(gotHeader, "application/json") {
				t.Errorf("got needed Content-Type header: %s;expected: %s", gotHeader, "application/json")
			}

			if gotBody := tt.args.resp.Body.String(); !strings.EqualFold(gotBody, tt.expected.body) {
				t.Errorf("got body: %s;expected: %s", gotBody, tt.expected.body)
			}
		})
	}
}
