package controller

import (
	"log"
	"net/http"
)

type (
	Controller interface {
		sleeper
	}
	sleeper interface {
		Sleep(w http.ResponseWriter, r *http.Request)
	}

	controller struct {
		logger *log.Logger
	}
)

func NewController(logger *log.Logger) Controller {
	return &controller{
		logger: logger,
	}
}
