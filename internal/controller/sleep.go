package controller

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab/netology-test-task/internal/models"
)

const (
	kb = 1 << 10
)

// Данный handlerFunc вызывает time.Sleep в своей gorutin-е
// time.Duration задается клиентом
func (c *controller) Sleep(w http.ResponseWriter, r *http.Request) {
	bytes, err := ioutil.ReadAll(io.LimitReader(r.Body, kb))
	if err != nil {
		c.logger.Printf("can't read requests body with error: %s\n", err.Error())
		writeError(w, `{"error":"invalidate request body"}`, http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	if len(bytes) == 0 {
		c.logger.Println("empty requests body")
		writeError(w, `{"error":"empty requests body"}`, http.StatusBadRequest)
		return
	}

	var t = new(models.Timeout)
	if err = json.Unmarshal(bytes, t); err != nil {
		c.logger.Printf("can't unmarshal requests body: %s\nwith error: %s\n", bytes, err.Error())
		writeError(w, `{"error":"invalidate json"}`, http.StatusBadRequest)
		return
	}

	if t.MilliSeconds != 0 {
		time.Sleep(time.Duration(t.MilliSeconds) * time.Millisecond)
	}

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusOK)

	if _, err := io.WriteString(w, `{"status":"ok"}`); err != nil {
		c.logger.Printf("can't write into response with error: %s\n", err.Error())
	}
}
