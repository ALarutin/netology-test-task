package application

import (
	"context"
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"time"

	"github.com/xlab/closer"

	"gitlab/netology-test-task/internal/controller"
	"gitlab/netology-test-task/internal/router"
	"gitlab/netology-test-task/pkg/config"
	"gitlab/netology-test-task/pkg/logger"
	"gitlab/netology-test-task/pkg/middleware"
)

type (
	Starter interface {
		Start()
	}

	application struct {
		opt *Options

		cfg *config.Application
		l   *log.Logger

		srv *http.Server
	}
)

func NewApplication(opt *Options) Starter {
	var (
		cfg  = config.NewConfig(fmt.Sprintf("./configs/%s.json", opt.Env))
		l    = logger.NewLogger(opt.LogOutputFilePath)
		ctrl = controller.NewController(l)
		mdl  = middleware.NewMiddleware(l, cfg.Application.Timeout.Cast())
		r    = router.NewRouter(ctrl, mdl)

		srv = &http.Server{
			Addr:    fmt.Sprintf("localhost:%d", opt.Port),
			Handler: r,
		}
	)

	return &application{
		opt: opt,
		cfg: &cfg.Application,
		l:   l,
		srv: srv,
	}
}

func (a *application) Start() {
	if a.opt.IsPprof {
		a.l.Println("turn on pprof")
		go func() {
			if err := http.ListenAndServe(":6060", nil); err != nil {
				a.l.Println(err)
			}
		}()
	}

	var c = make(chan os.Signal, 1)

	go func() {
		if err := a.srv.ListenAndServe(); err != nil {
			a.l.Println(err)
		}
	}()

	a.l.Printf("\nstart listening on port: %d\napplication:\n\tname: %s\n\tversion: %s\n\tenv: %s\n",
		a.opt.Port, a.cfg.Name, a.cfg.Version, a.opt.Env)

	signal.Notify(c, os.Interrupt)

	<-c

	ctx, cancel := context.WithTimeout(context.Background(), getShutdownTimeout(a.cfg.ShutdownTimeout.Cast()))
	defer cancel()

	if err := a.srv.Shutdown(ctx); err != nil {
		a.l.Printf("shut down error: %s", err.Error())
	}

	closer.Close()

	a.l.Println("shutting down")
	os.Exit(0)
}

const (
	defaultShutdownTimeout = time.Second
)

func getShutdownTimeout(duration time.Duration) time.Duration {
	if duration == 0 {
		return defaultShutdownTimeout
	}

	return duration
}
