package application

type (
	// Возможные настройки приложения
	Options struct {
		// Порт, который будет слушать приложение
		Port int
		// Окружение приложения
		Env string

		// Путь в файлу логгирования
		LogOutputFilePath string
		// Состояния вкл/выкл профилировщика
		IsPprof bool
	}
)
