package models

type (
	Timeout struct {
		MilliSeconds uint64 `json:"timeout"`
	}
)
